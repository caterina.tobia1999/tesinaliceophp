# TesinaLiceoPHP
This project contains a pdf showing the main features of the site and the files used to create the site written in HTML, CSS, PHP, JavaScript and SQLite.


# Project description
As a term paper for the scientific high school, applied sciences section, I brought a site for the TN1 scout group for which I volunteered 13 years. The site is made up of static pages describing the group and a dynamic page, i.e. a calendar where you can add customised events in which you indicate location, duration and activities. 