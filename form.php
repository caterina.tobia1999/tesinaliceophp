<html>

<head>
<link rel="shortcut icon" href="calendar.png"> 
<link rel="stylesheet" href="style.css">
<script src="script.js"></script>
</head>
<body>

<?php
// 1. collegamento al database; NB: manca copletamente la gestione delle eccezioni: db non esiste, struttura tabelle deiversa da quella attesa, ...
$db  = new PDO('sqlite:calendar.db');

// 2. l'identificativo dell'evento può essere passato come parametro della pagina, altrimenti viene impostato a -1 (nessun evento corrispondente)
$id = filter_input( INPUT_GET, 'id' ); if ($id == null) { $id = -1; }

// 3. posso passare un valore di data che è usato come default per ritrovo/rientro nel caso di nuovo evento
$d = filter_input( INPUT_GET, 'd' );
$m = filter_input( INPUT_GET, 'm' );
$y = filter_input( INPUT_GET, 'y' );

$nome_attivita = "";
$descrizione = "";
$tid = "";
$iid = "";

$query =  "SELECT NOME, DESCRIZIONE, TID, IID FROM EVENTI WHERE ID=".$id.";";
foreach ($db->query($query) as $row) // al massimo una riga
{	
	$nome_attivita = $row[0];
	$descrizione = $row[1];
	$tid = $row[2];
	$iid = $row[3];
}

echo "<p class='centered-text'><span class='important-text'>";
if ($nome_attivita == "") { echo "Inserisci nuova attività"; }
else  { echo "Modifica attività: \"".$nome_attivita."\""; }
echo "</span> Torna al <span class='label label-tag'><a href='calendar.php'>calendario</a></span></p>".PHP_EOL;
echo "<div class='flex-container'>".PHP_EOL;

?>

<form action="edit.php" method="post">
<input type="hidden" name="query" value="<?php if ($id == -1) { echo "insert"; } else { echo "update"; } ?>">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<div class="flex-container" id="container">
<div class="flex-item short">
	<label class="sticker" for="nome">Attività</label>
    <input type="text" name="nome" maxlength="100" required value="<?php echo htmlspecialchars($nome_attivita, ENT_QUOTES); ?>">
    <span>voce che compare nel calendario</span>
</div>
<div class="flex-item short">
    <label class="sticker" for="tipo">Tipologia</label>
	<select name="tipo" class="select-field" required>
	<?php if ( $tid == "" ) { echo "<option value='' disabled selected>...</option>"; } ?>
<?php	
$query =  "SELECT ID, DESCRIZIONE FROM TIPO_INCONTRI;";
foreach ($db->query($query) as $row)
{	
	echo "<option value='".$row[0]."'";
	if ($tid == $row[0]) echo " selected";
	echo ">".$row[1]."</option>".PHP_EOL;
}
?>
	</select>
    <span>tipologia di attività</span>
</div>
<div class="flex-item short">
    <label class="sticker" for="gruppo">Gruppo</label>
	<select name="gruppo" class="select-field" required>
	<?php if ( $iid == "" ) {	echo "<option value='' disabled  selected>...</option>"; } ?>
<?php	
$query =  "SELECT ID, DESCRIZIONE FROM INVITATI;";
foreach ($db->query($query) as $row)
{	
	echo "<option value='".$row[0]."'";
	if ($iid == $row[0]) echo " selected";
	echo ">".$row[1]."</option>".PHP_EOL;
}
?>
	</select>
    <span>gruppo coinvolto nell'attività</span>
</div>
<div class="flex-item no-border forced-to-right">
	<input type="submit" value="salva">
</div>
<div class="flex-item long">
    <label class="sticker" for="desc">Descrizione</label>
    <textarea name="descrizione" wrap="hard" onkeyup="adjust_textarea(this)"><?php echo $descrizione; ?></textarea>
    <span>informazioni relative all'evento</span>
</div>

<?php	
if ( $id != -1 )
{		
	$query =  "SELECT DATA_RITROVO, LUOGO_RITROVO, LUOGO_SVOLGIMENTO, DATA_RIENTRO, LUOGO_RIENTRO FROM INCONTRI WHERE EID=".$id.";";
	$counter = 1;
	foreach ($db->query($query) as $row)
	{	
		$data_ritrovo=DateTime::createFromFormat('Y-m-d H:i', $row[0]);
		$luogo_ritrovo=$row[1];
		$luogo_svolgimento=$row[2];
		$data_rientro=DateTime::createFromFormat('Y-m-d H:i', $row[3]);
		$luogo_rientro=$row[4];

		$alt = "";
		if ( $counter%2 == 0) $alt = " class='alt'";
		echo "<div class='flex-item short meeting'>".PHP_EOL;
		echo "<label class='sticker' for='data_ritrovo[]'>Ritrovo</label>".PHP_EOL;
		echo "<input type='date' name='data_ritrovo[]' required value='".$data_ritrovo->format("Y-m-d")."'><input type='time' name='ora_ritrovo[]' required value='".$data_ritrovo->format("H:i")."'>".PHP_EOL;
		echo "<span".$alt.">incontro ".$counter++.": giorno e ora di ritrovo</span>".PHP_EOL;
		echo "</div>".PHP_EOL;
		
		echo "<div class='flex-item short meeting'>".PHP_EOL;
		echo "<label class='sticker' for='luogo_ritrovo[]'>Ritrovo</label>".PHP_EOL;
		echo "<input type='text' name='luogo_ritrovo[]' maxlength='100' required value='".htmlspecialchars($luogo_ritrovo, ENT_QUOTES)."'>".PHP_EOL; //PROBLEMA: usa htmlspecialchars_decode per evitare che "port'aquila" o "piazza vicenza", se aggiorno l'evento senza cambiarli, vengano salvati male
		echo "<span".$alt.">luogo di ritrovo</span>".PHP_EOL;
		echo "</div>".PHP_EOL;
		
		echo "<div class='flex-item short meeting'>".PHP_EOL;
		echo "<label class='sticker' for='luogo_svolgimento[]'>Svolgimento</label>".PHP_EOL;
		echo "<input type='text' name='luogo_svolgimento[]' maxlength='100' required value='".htmlspecialchars($luogo_svolgimento, ENT_QUOTES)."'>".PHP_EOL;
		echo "<span".$alt.">luogo di svolgimento</span>".PHP_EOL;
		echo "</div>".PHP_EOL;

		echo "<div class='flex-item short meeting'>".PHP_EOL;
		echo "<label class='sticker' for='data_rientro[]'>Rientro</label>".PHP_EOL;
		echo "<input type='date' name='data_rientro[]' required value='".$data_rientro->format("Y-m-d")."'><input type='time' name='ora_rientro[]' required value='".$data_ritrovo->format("H:i")."'>".PHP_EOL;
		echo "<span".$alt.">giorno e ora di rientro</span>".PHP_EOL;
		echo "</div>".PHP_EOL;
		
		echo "<div class='flex-item short meeting'>".PHP_EOL;
		echo "<label class='sticker' for='luogo_rientro[]'>Rientro</label>".PHP_EOL;
		echo "<input type='text' name='luogo_rientro[]' maxlength='100' required value='".htmlspecialchars($luogo_rientro, ENT_QUOTES)."'>".PHP_EOL;
		echo "<span".$alt.">luogo di ritrovo</span>".PHP_EOL;
		echo "</div>".PHP_EOL;
	}
} else {	
	$data_default = "";
	if ($d != null && $m != null && $y != null) { $data_default = $y."-".$m."-".$d; }
?>
<div class="flex-item short meeting">
    <label class="sticker" for="data_ritrovo[]">Ritrovo</label>
	<input type="date" name="data_ritrovo[]" required value="<?php echo $data_default; ?>"><input type="time" name="ora_ritrovo[]" required>
    <span>incontro 1: giorno e ora di ritrovo</span>
</div>
<div class="flex-item short meeting">
    <label class="sticker" for="luogo_ritrovo[]">Ritrovo</label>
    <input type="text" name="luogo_ritrovo[]" maxlength="100" required>
    <span>luogo di ritrovo</span>
</div>
<div class="flex-item short meeting">
    <label class="sticker" for="luogo_svolgimento[]">Svolgimento</label>
    <input type="text" name="luogo_svolgimento[]" maxlength="100" required>
    <span>luogo di svolgimento</span>
</div>
<div class="flex-item short meeting">
    <label class="sticker" for="data_rientro[]">Rientro</label>
	<input type="date" name="data_rientro[]" required value="<?php echo $data_default; ?>"><input type="time" name="ora_rientro[]" required">
    <span>giorno e ora di rientro</span>
</div>
<div class="flex-item short meeting">
    <label class="sticker" for="luogo_rientro[]">Rientro</label>
    <input type="text" name="luogo_rientro[]" maxlength="100" required>
    <span>luogo di rientro</span>
</div>
<?php
}
?>

<div class="flex-item short clickable centered-text" onclick="addMeeting()">
	<label class="sticker">aggiungi incontro</label>
	<div class="label-inverse label-theme-inverse"><big>+</big></div>
</div>
<div class="flex-item short clickable centered-text forced-to-right" onclick="removeLastMeeting()">
	<label class="sticker">rimuovi ultimo incontro</label>
	<div class="label-inverse label-theme-inverse"><big>-</big></div>
</div>
</div>
</form>

</body>
</html>