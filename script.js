
//auto expand textarea
function adjust_textarea(h) {
    h.style.height = "100px";
    h.style.height = (h.scrollHeight)+"px";
}

function addMeeting(){
	var container = document.getElementById('container');
	var n = document.getElementsByClassName("meeting").length / 5;
	var altClass="";
	if (n%2) altClass=" class='alt'";
	
	var pointer = container.lastElementChild.previousElementSibling;
	
	var entry1 = document.createElement('div');
	entry1.className += ' flex-item short meeting';
	entry1.innerHTML = "<label class='sticker' for='data_ritrovo[]'>Ritrovo</label><input type='date' name='data_ritrovo[]' required><input type='time' name='ora_ritrovo[]' required><span" + altClass + ">incontro "+(n+1)+ ": giorno e ora di ritrovo</span>";
	container.insertBefore(entry1, pointer);

	var entry2 = document.createElement('div');
	entry2.className += ' flex-item short meeting';
	entry2.innerHTML = "<label class='sticker' for='luogo_ritrovo[]'>Ritrovo</label><input type='text' name='luogo_ritrovo[]' maxlength='100' required><span" + altClass + ">luogo di ritrovo</span>";
	container.insertBefore(entry2, pointer);
	
	var entry3 = document.createElement('div');
	entry3.className += ' flex-item short meeting';
	entry3.innerHTML = "<label class='sticker' for='luogo_svolgimento[]'>Svolgimento</label><input type='text' name='luogo_svolgimento[]' maxlength='100' required><span" + altClass + ">luogo di svolgimento</span>";
	container.insertBefore(entry3, pointer);

	var entry4 = document.createElement('div');
	entry4.className += ' flex-item short meeting';
	entry4.innerHTML = "<label class='sticker' for='data_rientro[]'>Rientro</label><input type='date' name='data_rientro[]' required><input type='time' name='ora_rientro[]' required><span" + altClass + ">giorno e ora di rientro</span>";
	container.insertBefore(entry4, pointer);

	var entry5 = document.createElement('div');	
	entry5.className += ' flex-item short meeting';
	entry5.innerHTML = "<label class='sticker' for='luogo_rientro[]'>Rientro</label><input type='text' name='luogo_rientro[]' maxlength='100' required><span" + altClass + ">luogo di rientro</span>";
	container.insertBefore(entry5, pointer);
}

function removeLastMeeting()
{
	var container = document.getElementById('container');
	var meeting = document.getElementsByClassName("meeting");
	var n = meeting.length;
	if (n <= 5) return;
	for (i = 1; i <= 5; ++i) { container.removeChild( meeting[n-i] ); }
}