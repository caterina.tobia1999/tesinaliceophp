<html>
<head>
<link rel="shortcut icon" href="calendar.png"> 
<link rel="stylesheet" href="style.css">
</head>
<body>

<?php
// 1. collegamento al database; NB: manca copletamente la gestione delle eccezioni: db non esiste, struttura tabelle deiversa da quella attesa, ...
$db  = new PDO('sqlite:calendar.db');

// 2. viene creata una data di riferimento su cui costruire il calendario; mese e anno possono essere passati come parametri della pagina, altrimenti vengono utilizzati quelli correnti
$m = filter_input( INPUT_GET, 'm' ); if ($m == null) $m = date("m");
$y = filter_input( INPUT_GET, 'y' ); if ($y == null) $y = date("Y");
$dt = DateTime::createFromFormat('Y-m-d', $y."-".$m."-02"); // la data impostata al giorno 2 perché il calendario viene fatto partire dal lunedì precedente (se è il primo, non devo andare indietro di un'altra settimana)

// 3. costruzione della tabella (calendario)
echo "<center>".PHP_EOL;
echo "<table class='calendar'>".PHP_EOL;
// 3.1 riga di navigazione: mostra il mese visualizzato e le frecce per passare al precedente e successivo
echo "<tr>".PHP_EOL;
echo "<th colspan='3' class='rightAlign'><div class='label label-theme'><a href='calendar.php?y=".(clone $dt)->modify('previous month')->format('Y')."&m=".(clone $dt)->modify('previous month')->format('m')."'> < </a></div></th>".PHP_EOL;
setlocale(LC_ALL, 'ita'); // poco elegante, usa un altro formato per le date, ma non sembra ci siano altri modi per ottenere il nome del mese in italiano
echo "<th><div class='label label-theme'>".strftime("%B", mktime(0, 0, 0, $m, 1, $y))." ".$y."</div></th>".PHP_EOL;
echo "<th colspan='3' class='leftAlign'><div class='label label-theme'><a href='calendar.php?y=".(clone $dt)->modify('next month')->format('Y')."&m=".(clone $dt)->modify('next month')->format('m')."'> > </a></div></th>".PHP_EOL;
echo "</tr>".PHP_EOL;
// 3.2 riga informativa con i giorni della settimana (lu - do)
echo "<tr><th class='calendar'>lu</th><th class='calendar'>ma</th><th class='calendar'>me</th><th class='calendar'>gi</th><th class='calendar'>ve</th><th class='calendar'>sa</th><th class='calendar'>do</th></tr>".PHP_EOL;

// 3.3 calendario vero e proprio: si alternano una riga con il n. del giorno e una con gli appuntamenti; 6 righe per 6 settimane complete, in modo che il mese venga sempre mostrato per intero (in testa e coda avremo giorni dei mesi adiacenti) 
// 3.3.1 comincia dal primo lunedì valido
$dt->modify('previous monday');
for ($r = 0; $r < 6; $r++) 
{
// riga che contiene il n. del giorno
	echo "<tr class='calendar'>".PHP_EOL;
	for ($c = 0; $c < 7; $c++) 
	{    
		echo "<td class='calendar";
// se appartiene ad un mese diverso da quello selezionato aggiunta classe per cambiare lo stile (-> colore)
		if ($dt->format('m')!=$m) echo " dark-shadow";
		echo "'><div class='cell-wrapper'><a href='detail.php?y=".$dt->format('Y')."&m=".$dt->format('m')."&d=".$dt->format('d')."' class='wrapper-item-boss'>".$dt->format('d')."</a><a href='form.php?y=".$dt->format('Y')."&m=".$dt->format('m')."&d=".$dt->format('d')."'><div class='label-inverse label-theme-inverse label-dwarf' title='aggiungi attività'>+</div></a></div></td>".PHP_EOL;
		$dt->modify('next day'); //style='margin-left: auto;'
	} 
	echo "</tr>".PHP_EOL;
	$dt->modify('previous monday');
	echo "<tr class='calendar'>".PHP_EOL;
	for ($c = 0; $c < 7; $c++) 
	{    
		echo "<td class='calendar";
		if ($dt->format('m')!=$m) echo " light-shadow";
		echo "'>".PHP_EOL;;

		// sembra impossibile ma non trovo un modo per farmi ridare il numero dei record trovati nella select...
		$nEvents = $db->query("SELECT COUNT(*) FROM EVENTI E, TIPO_INCONTRI TI, INVITATI INV, INCONTRI INC 
		WHERE INC.EID=E.ID and E.TID=TI.ID and E.IID=INV.ID and DATETIME(INC.DATA_RITROVO) < '".$dt->format('Y-m-d 23:59')."' and DATETIME(INC.DATA_RIENTRO) > '".$dt->format('Y-m-d 00:01')."'")->fetchColumn();

		$query =  "SELECT E.NOME, E.ID, TI.DESCRIZIONE, INV.DESCRIZIONE, INC.LUOGO_RITROVO, INC.LUOGO_RIENTRO, INC.LUOGO_SVOLGIMENTO, INC.DATA_RITROVO, INC.DATA_RIENTRO  
		FROM EVENTI E, TIPO_INCONTRI TI, INVITATI INV, INCONTRI INC 
		WHERE INC.EID=E.ID and E.TID=TI.ID and E.IID=INV.ID and DATETIME(INC.DATA_RITROVO) < '".$dt->format('Y-m-d 23:59')."' and DATETIME(INC.DATA_RIENTRO) > '".$dt->format('Y-m-d 00:01')."'
		ORDER BY INC.DATA_RITROVO;";
		
		$counter = 1;
		$maxEvents=3;
		if ($nEvents > 0) 
		{  	
			echo "<div class='cell-wrapper'>";
			foreach ($db->query($query) as $row)
			{
				if ($nEvents > $maxEvents && $counter++ >= $maxEvents) break; // involuto perche' se il numero eventi supera il massimo possibile devo toglierne 2 (una riga mi serve per mettere il n. di eventi mancanti)
				
				$attivita=$row[0];
				$id_evento=$row[1];
				$tipologia=$row[2];
				$gruppo=$row[3];
				$luogo_ritrovo=$row[4];
				$luogo_rientro=$row[5];
				$luogo_svolgimento=$row[6];
				$data_ritrovo=DateTime::createFromFormat('Y-m-d H:i', $row[7]);
				$data_rientro=DateTime::createFromFormat('Y-m-d H:i', $row[8]);

				echo "<div class='wrapper-item-boss'><div class='tooltip'>".PHP_EOL;
				echo "<a href='detail.php?id=".$id_evento."'><div class='label label-calendar-entry label-".$gruppo."'>";
				if ($data_ritrovo->format('m-d') != $dt->format('m-d')) { echo "&emsp;&emsp;&ensp;"; } else { echo $data_ritrovo->format("H:i"); }
				echo " ".$attivita."</div></a><span class='tooltiptext tooltiptext-";
				if ($r != 5) { echo "below-"; } else { echo "above-"; } 
				if ($c < 4) { echo "right'>".PHP_EOL; } else { echo "left'>".PHP_EOL; } 
				echo "<table class='tooltiptext-info'>".PHP_EOL;
				echo "<tr><td class='rightAlign'><div class='label label-theme'>attività:</div></td><td class='leftAlign'><div class='label label-tag'>".$attivita."</div></td></tr>".PHP_EOL;
				echo "<tr><td class='rightAlign'><div class='label label-theme'>gruppo:</div></td><td class='leftAlign'><div class='label label-".$gruppo."'>".$gruppo."</div></td></tr>".PHP_EOL;
				echo "<tr><td class='rightAlign'><div class='label label-theme'>ritrovo:</div></td><td class='leftAlign'><div class='label label-tag'>".$data_ritrovo->format("d/m H:i")."</div><div class='label label-tag'>".$luogo_ritrovo."</div></td></tr>".PHP_EOL;
				echo "<tr><td class='rightAlign'><div class='label label-theme'>svolgimento:</div></td><td class='leftAlign'><div class='label label-tag'>".$luogo_svolgimento."</div></td></tr>".PHP_EOL;
				echo "<tr><td class='rightAlign'><div class='label label-theme'>rientro:</div></td><td class='leftAlign'><div class='label label-tag'>".$data_rientro->format("d/m H:i")."</div><div class='label label-tag'>".$luogo_rientro."</div></td></tr>".PHP_EOL;
				echo "</table>".PHP_EOL;
				echo "</span>".PHP_EOL;
				echo "</div></div>".PHP_EOL;
				echo "<a href='form.php?id=".$id_evento."'><img src='edit.png' title='modifica'></a><a href='edit.php?query=delete&id=".$id_evento."' onclick=\"return confirm('Eliminare attività?')\"><img src='trash.png' title='elimina'></a>".PHP_EOL;
			}
			if ($nEvents > $maxEvents) echo "<a href='detail.php?y=".$dt->format('Y')."&m=".$dt->format('m')."&d=".$dt->format('d')."' class='wrapper-item-boss'><div class='label label-important'>+".($nEvents-$maxEvents+1)." eventi</div></a>";	
			
			echo "</div>".PHP_EOL;
		}
		
		$dt->modify('next day');
	} 
	echo "</tr>".PHP_EOL;	
} 

echo "</table>".PHP_EOL;
echo "</center>".PHP_EOL;
?>

</body>
</html>