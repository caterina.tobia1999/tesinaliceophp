<html>

<head>
<link rel="shortcut icon" href="calendar.png"> 
<link rel="stylesheet" href="style.css">

<style>
.top-border { border-top: 1px dashed black; }
</style>

</head>
<body>

<?php

// 1. collegamento al database; NB: manca copletamente la gestione delle eccezioni: db non esiste, struttura tabelle deiversa da quella attesa, ...
$db  = new PDO('sqlite:calendar.db');

// 2. l'identificativo dell'evento può essere passato come parametro della pagina
$id = filter_input( INPUT_GET, 'id' );

// 3. posso passare un valore di data che è usato come default per ritrovo/rientro nel caso non abbia un evento di riferimento
$d = filter_input( INPUT_GET, 'd' );
$m = filter_input( INPUT_GET, 'm' );
$y = filter_input( INPUT_GET, 'y' );

$query = "";
if ($id != null) { $query = "SELECT E.NOME, E.ID, E.DESCRIZIONE, TI.DESCRIZIONE, INV.DESCRIZIONE FROM EVENTI E, TIPO_INCONTRI TI, INVITATI INV WHERE E.TID=TI.ID and E.IID=INV.ID and E.ID=".$id; }
else if ($d != null && $m != null && $y != null) 
{ 
	$dt = DateTime::createFromFormat('Y-m-d', $y."-".$m."-".$d);
	$query = "SELECT E.NOME, E.ID, E.DESCRIZIONE, TI.DESCRIZIONE, INV.DESCRIZIONE FROM EVENTI E, TIPO_INCONTRI TI, INVITATI INV, INCONTRI INC WHERE E.TID=TI.ID and E.IID=INV.ID and INC.EID=E.ID and DATETIME(INC.DATA_RITROVO) < '".$dt->format('Y-m-d 23:59')."' and DATETIME(INC.DATA_RIENTRO) > '".$dt->format('Y-m-d 00:01')."' ORDER BY INC.DATA_RITROVO";
}
echo "<p class='centered-text'><span class='important-text'>Dettaglio attività";
if ($id != null) { echo ": \"".$db->query("SELECT NOME FROM EVENTI WHERE ID=".$id.";")->fetchColumn()."\""; }
else if ($d != null && $m != null && $y != null)  { echo " del ".$d.".".$m.".".$y; }
echo "</span> Torna al <span class='label label-tag'><a href='calendar.php'>calendario</a></span></p>".PHP_EOL;
echo "<div class='flex-container'>".PHP_EOL;

foreach ($db->query($query) as $row)
{	
	$attivita = $row[0];
	$id = $row[1];
	$descrizione = $row[2];
	$tipologia = $row[3];
	$gruppo = $row[4];

	echo "<div class='flex-item medium'>".PHP_EOL;
	echo "<div class='sticker'><a href='form.php?id=".$id."'><img src='edit.png' title='modifica'></a><a href='edit.php?query=delete&id=".$id."' onclick=\"return confirm('Eliminare attività?')\"><img src='trash.png' title='elimina'></a><div style='margin-left: 15px;'>Attività: ".$attivita."</div></div>".PHP_EOL;

	echo "<table class='detail'>".PHP_EOL;
	//echo "<tr><td class='rightAlign'><div class='label label-theme'>attività:</div></td><td class='leftAlign'>".$attivita."</td></tr>".PHP_EOL;
	echo "<tr><td class='rightAlign'><div class='label label-".$gruppo."'>gruppo:</div></td><td class='leftAlign'>".$gruppo."</td></tr>".PHP_EOL;
	echo "<tr><td class='rightAlign'><div class='label label-theme'>tipologia:</div></td><td class='leftAlign'>".$tipologia."</td></tr>".PHP_EOL;
	echo "<tr><td class='rightAlign'><div class='label label-theme'>descrizione:</div></td><td class='leftAlign'>".$descrizione."</td></tr>".PHP_EOL;

	$n = $db->query("SELECT COUNT(*) FROM INCONTRI WHERE EID=".$id.";")->fetchColumn();
	$nested_query =  "SELECT LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO FROM INCONTRI WHERE EID=".$id.";";
	$counter = 1;
	foreach ($db->query($nested_query) as $nested_row)
	{	
		$luogo_ritrovo = $nested_row[0];
		$luogo_rientro = $nested_row[1];
		$luogo_svolgimento = $nested_row[2];
		$data_ritrovo = DateTime::createFromFormat('Y-m-d H:i', $nested_row[3]);
		$data_rientro = DateTime::createFromFormat('Y-m-d H:i', $nested_row[4]);
		echo "<tr><td class='rightAlign'><div class='label label-theme-inverse'>incontro";
		if ($n > 1) echo " ".$counter++;
		echo ":</div></td><td class='leftAlign'></td></tr>".PHP_EOL;
		echo "<tr><td class='rightAlign'><div class='label label-theme'>ritrovo:</div></td><td class='leftAlign'>".$data_ritrovo->format("d/m H:i")." ".$luogo_ritrovo."</td></tr>".PHP_EOL;
		echo "<tr><td class='rightAlign'><div class='label label-theme'>svolgimento:</div></td><td class='leftAlign'>".$luogo_svolgimento."</td></tr>".PHP_EOL;
		echo "<tr><td class='rightAlign'><div class='label label-theme'>rientro:</div></td><td class='leftAlign'>".$data_rientro->format("d/m H:i")." ".$luogo_rientro."</td></tr>".PHP_EOL;
	}
	echo "</table>".PHP_EOL;

	echo "</div>".PHP_EOL;
	
}

echo "</div>".PHP_EOL;

?>

</body>
</html>