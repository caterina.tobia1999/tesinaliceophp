<html>

<head>
<link rel="shortcut icon" href="calendar.png"> 
<link rel="stylesheet" href="style.css">
</head>
<body>

<?php

$query = filter_input( INPUT_POST, 'query' );
if ( $query == null) $query = filter_input( INPUT_GET, 'query' );

$id = filter_input( INPUT_POST, 'id' );
if ( $id == null) $id = filter_input( INPUT_GET, 'id' );

$nome = filter_input( INPUT_POST, 'nome' );
$tipo = filter_input( INPUT_POST, 'tipo' );
$gruppo = filter_input( INPUT_POST, 'gruppo' );
$descrizione = filter_input( INPUT_POST, 'descrizione' );

$data_ritrovo = filter_input( INPUT_POST, 'data_ritrovo', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$ora_ritrovo = filter_input( INPUT_POST, 'ora_ritrovo', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$luogo_ritrovo = filter_input( INPUT_POST, 'luogo_ritrovo', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$luogo_svolgimento = filter_input( INPUT_POST, 'luogo_svolgimento', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$data_rientro = filter_input( INPUT_POST, 'data_rientro', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$ora_rientro = filter_input( INPUT_POST, 'ora_rientro', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
$luogo_rientro = filter_input( INPUT_POST, 'luogo_rientro', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );

$db  = new PDO('sqlite:calendar.db');

if ( $query == "update" || $query == "delete")
{	
	$db->query("DELETE FROM INCONTRI WHERE EID=".$id.";");
	$db->query("DELETE FROM EVENTI WHERE ID=".$id.";");
} 	
if ( $query == "insert" || $query == "update")
{	
	$db->query("INSERT INTO EVENTI (TID,IID,NOME,DESCRIZIONE) values (".$tipo.",".$gruppo.",".$db->quote( htmlspecialchars_decode ( $nome, ENT_QUOTES ) ).", ".$db->quote( htmlspecialchars_decode ( $descrizione, ENT_QUOTES ) ).");");
	$eid = $db->lastInsertId();
	for ($i=0;$i<count($luogo_ritrovo);$i++)
	{		
		$db->query("INSERT INTO INCONTRI (EID,LUOGO_RITROVO,LUOGO_RIENTRO,LUOGO_SVOLGIMENTO,DATA_RITROVO,DATA_RIENTRO) 
		values (".$eid.",".$db->quote( htmlspecialchars_decode ( $luogo_ritrovo[$i], ENT_QUOTES ) ).",".$db->quote( htmlspecialchars_decode ( $luogo_rientro[$i], ENT_QUOTES ) ).",".$db->quote( htmlspecialchars_decode ( $luogo_svolgimento[$i], ENT_QUOTES ) ).",'"
		.$data_ritrovo[$i]." ".$ora_ritrovo[$i]."','".$data_rientro[$i]." ".$ora_rientro[$i]."');");
	} 
} 

echo "<p class='centered-text'><span class='important-text'>Attività ";
if ( $query == "update" ) { echo "aggiornata"; }
if ( $query == "delete" ) { echo "eliminata"; }
if ( $query == "insert" ) { echo "inserita"; }
echo "</span> Torna al <span class='label label-tag'><a href='calendar.php'>calendario</a></span></p>".PHP_EOL;

?>

</body>
</html>