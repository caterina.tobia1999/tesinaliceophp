drop table if exists TIPO_INCONTRI;
create table TIPO_INCONTRI(
ID INTEGER PRIMARY KEY, 
DESCRIZIONE VARCHAR (255) NOT NULL);

drop table if exists INVITATI;
create table INVITATI(
ID INTEGER PRIMARY KEY, 
DESCRIZIONE VARCHAR (255) NOT NULL);

drop table if exists EVENTI;
create table EVENTI(
ID INTEGER PRIMARY KEY, 
TID INTEGER NOT NULL, 
IID INTEGER NOT NULL, 
NOME VARCHAR (255) NOT NULL, 
DESCRIZIONE VARCHAR (255), 
FOREIGN KEY (TID) REFERENCES TIPO_INCONTRI(ID), 
FOREIGN KEY (IID) REFERENCES INVITATI(ID));

drop table if exists INCONTRI;
create table INCONTRI(
ID INTEGER PRIMARY KEY, 
EID INTEGER NOT NULL, 
LUOGO_RITROVO VARCHAR(255) NOT NULL, 
LUOGO_RIENTRO VARCHAR(255) NOT NULL, 
LUOGO_SVOLGIMENTO VARCHAR(255) NOT NULL, 
DATA_RITROVO TIMESTAMP NOT NULL, 
DATA_RIENTRO TIMESTAMP NOT NULL, 
FOREIGN KEY (EID) REFERENCES EVENTI(ID));


insert into TIPO_INCONTRI (ID, DESCRIZIONE) values (1, "campo");
insert into TIPO_INCONTRI (ID, DESCRIZIONE) values (2, "attività");
insert into TIPO_INCONTRI (ID, DESCRIZIONE) values (3, "volontariato");
insert into TIPO_INCONTRI (ID, DESCRIZIONE) values (4, "autofinanziamento");

insert into INVITATI (ID, DESCRIZIONE) values (1, "lupetti");
insert into INVITATI (ID, DESCRIZIONE) values (2, "reparto");
insert into INVITATI (ID, DESCRIZIONE) values (3, "clan");

insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (1, 1, 3, "route", "giro in spagna; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");
insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (2, 2, 1, "caccia di primavera", "caccia lupetto primavera; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");
insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (3, 2, 2, "giro 'al sas'", "camminata in montagna; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");
insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (4, 3, 3, "strada", "aiuto senza tetto; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");
insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (5, 4, 2, "parcheggi", "servizio parcheggi per feste; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");
insert into EVENTI (ID, TID, IID, NOME, DESCRIZIONE) values (6, 4, 1, "vendita calendari", "vendita calendari porta a porta; qui andrebbe inserito il programma, di modo da far diventare lunga la descrizione dell'attività...");

insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (10, 1, "trento, stazione dei treni", "verona, areoporto", "spagna", "2017-06-25 10:00", "2017-07-09 15:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (20, 2, "sede di trento", "sede di trento", "trento", "2017-05-10 14:00", "2017-05-10 16:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (21, 2, "sede di trento", "sede di trento", "trento", "2017-05-13 14:00", "2017-05-13 16:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (22, 2, "sede di trento", "sede di trento", "trento", "2017-05-20 15:00", "2017-05-20 17:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (23, 2, "sede di trento", "sede di trento", "trento", "2017-05-25 15:00", "2017-05-25 17:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (30, 3, "port'aquila", "port'aquila", "trento", "2017-05-20 08:00", "2017-05-20 17:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (31, 3, "port'aquila", "port'aquila", "trento", "2017-04-26 08:00", "2017-04-26 17:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (40, 4, "via brennero", "via brennero", "trento", "2017-05-15 20:00", "2017-05-15 23:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (41, 4, "via brennero", "via brennero", "trento", "2017-05-20 20:00", "2017-05-20 23:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (42, 4, "via brennero", "via brennero", "trento", "2017-05-22 20:00", "2017-05-22 23:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (50, 5, "da Vinci", "da Vinci", "trento", "2017-05-20 12:00", "2017-05-20 18:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (51, 5, "Galilei", "Galilei", "trento", "2017-05-25 12:00", "2017-05-25 18:30");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (61, 6, "trento, sede", "trento, sede", "trento città", "2017-05-15 14:00", "2017-05-15 18:00");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (62, 6, "trento, sede", "trento, sede", "trento città", "2017-05-20 14:00", "2017-05-20 18:00");
insert into INCONTRI (ID, EID, LUOGO_RITROVO, LUOGO_RIENTRO, LUOGO_SVOLGIMENTO, DATA_RITROVO, DATA_RIENTRO) values (63, 6, "trento, sede", "trento, sede", "trento città", "2017-05-25 14:00", "2017-05-25 18:00");

/* IMPORTANTE: le date devono essere inserite con il giusto formato, es: 2017-05-20 09:00 e non 2017-05-20 9:00 altrimenti le query non funzionano!! */
